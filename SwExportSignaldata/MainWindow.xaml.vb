﻿Class MainWindow

    Private Sub ExportSignalDataXML_Closed(sender As Object, e As EventArgs)
        Me.Close()
    End Sub

    Private Sub MnExit_Click(sender As Object, e As RoutedEventArgs)
        Me.Close()
    End Sub

#Region "<!--Meny Skapa leverans till projekt ANDA-->"
    Private Sub MnANDA_Click(sender As Object, e As RoutedEventArgs)
        Dim ProjectExportBIS1 As New ProjectExportBIS()
        ProjectExportBIS1.ShowDialog()
    End Sub
#End Region

#Region "<!--Meny Om Genereing XML Data Signal-->"
    Private Sub MnAboutGenereringXMLDataSignal_Click(sender As Object, e As RoutedEventArgs)
        Dim ownedWindow As New AboutGenereringXMLDataSignal()
        ownedWindow.Owner = Me
        ownedWindow.ShowDialog()
    End Sub
#End Region

#Region "<!--Meny Skapa leverans till Trafikverket, typ Signalställverk-->"
    Private Sub MnXMLTrvSwedenInterlocking_Click(sender As Object, e As RoutedEventArgs)
        Dim OpenProjectVtool1 As New OpenProjectVtool()
        OpenProjectVtool1.ShowDialog()
    End Sub
#End Region

End Class
