﻿Imports System.IO

Module Basicfunctions

#Region "Public"
    Public MainFolder As String = "C:\temp\SwecoExportSignalDataXML"
    Public VtoolsDataSet As VToolsInmatningDataSet
    Public VtoolsDataAdaper As VToolsInmatningDataSetTableAdapters.TableAdapterManager
    Public VtoolsbacktobackDataAdapter As VToolsInmatningDataSetTableAdapters.BacktoBackTableAdapter
#End Region

#Region "Private"
    Private ReadOnly ReportFileName As String = "Report.txt"
    Private ReadOnly ChangeLog As String = "ChangeLog.txt"
#End Region

#Region "Error Report"
    'Create Templates directory
    Public Sub CreateErrorReportdirectory()
        'create Licens directory
        Dim di As DirectoryInfo = New DirectoryInfo(MainFolder & "\")
        Try
            ' Determine whether the directory exists.
            If di.Exists Then
                Return
            End If

            ' Try to create the directory.
            di.Create()

        Catch e As Exception
            MakeErrorReport("Create directory ErrorReport file. ", e)
            MessageBox.Show("The process failed: {0}", e.ToString())
        End Try
    End Sub

    'Create File
    Public Sub CreateErrorReport()
        Try
            If File.Exists(MainFolder & "\" & ReportFileName) Then
                File.Delete(MainFolder & "\" & ReportFileName)
            End If

            File.Create(MainFolder & "\" & ReportFileName)

        Catch ex As Exception
            Throw New ApplicationException("CreateErrorReportFile: ", ex)
        End Try
    End Sub

    'Error Report
    Public Sub MakeErrorReport(FunctionName As String, ex As System.Exception)
        Dim TxtString As String
        TxtString = FunctionName & " " & ex.Message & " ; Fel loggat av:" & Environment.UserName & " ,datum :" & System.DateTime.Now & Environment.NewLine
        File.AppendAllText(MainFolder & "\" & ReportFileName, TxtString)
    End Sub
#End Region


#Region "MakeErrorReport WithoutEx"
    Public Sub MakeErrorReport_WithoutEx(FunctionName As String, ErrorString As String)
        Dim TxtString As String
        TxtString = FunctionName & " " & ErrorString & " ; Fel loggat av:" & Environment.UserName & " ,datum :" & System.DateTime.Now & Environment.NewLine
        File.AppendAllText(MainFolder & "\" & ReportFileName, TxtString)
    End Sub
#End Region

#Region "MakeLogUpdate"
    Public Sub MakeLogUpdate(Action As String)
        Dim TxtString As String
        TxtString = Action & Environment.NewLine
        File.AppendAllText(MainFolder & "\" & ChangeLog, TxtString)
    End Sub
#End Region


End Module
