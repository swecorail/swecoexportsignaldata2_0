﻿Imports System.IO
Imports Microsoft.Office.Interop

Module ConfigurationBIS

#Region "Public"
    Public OleDbConnection_SwTmovesDatabase As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" 'OleDbConnection
    Public ProjectSwTmoves_Database As String = "" 'databasuppkoppling Temporär TMoves databas
    Public QualityControl_ExcelFileTemplate As String = "\\segvxfs001\PROJEKT\7656\7501425\000\3. Genomförande (E)\3.1 Arbetsmaterial\Mallar Leverans XML\Kvalitetskontroll 1.0.xlsx" 'T-disk
    Public QualityControl_ExcelFile As String = ""
    Public QualityControl_xlWorksheet As Excel.Worksheet 'flik i excelfil
    Public QualityControl_Counter As Integer = 1
    Public QualityControl_xlApp As New Excel.Application 'starta applikation exel
    Public QualityControl_xlWorkBook As Excel.Workbook ' excelfil

    Public ProjectDirectory As String = "" 'C:\ selected project directory mapp
    Public OnlyFileNameProject As String = "" ' filnamn.xml
    Public newdatabasSwTmoves As String = "" 'databas projekt ANDA
    Public DefultdatabasSwTmoves = ProjectDirectory & "\" & OnlyFileNameProject & "\Projekteringsunderlag" & "\Anda.accdb"
    Public CopyDatabasSwTmoves As String = "\\segvxfs001\PROJEKT\7656\7501425\000\3. Genomförande (E)\3.1 Arbetsmaterial\Databas\Anda.accdb" 'T-disk
    'Public CopyDatabasSwTmoves As String = "C:\VisualStudio\Projects\SwExportSignaldata\Utskrift\Databas ANDA\Anda.accdb" 'ANDA orginal
    Public newfileProofOfDelivery As String = ""
    Public CopyListOperationSite As String = "\\segvxfs001\PROJEKT\7656\7501425\000\3. Genomförande (E)\3.1 Arbetsmaterial\Mallar Leverans XML\Platser bandel VTools.xlsx" 'T-disk" 
    Public NewListOperationSite As String = "" 'Vtool lista
    Public ListOperationSite_xlWorkBook As Excel.Workbook ' excelfil bandelar
    Public ListOperationSite_xlWorksheet As Excel.Worksheet 'flik i excelfil bandelar
    Public Folder_XMLdelivery As String = "XML Leverans" 'Mappnamn för XML filer som ska ingå i leverans
    Public LathundExportverktygXML As String = "\\segvxfs001\PROJEKT\7656\7501425\000\3. Genomförande (E)\3.1 Arbetsmaterial\Mallar Leverans XML\Lathund exportverktyg XML-filer från VTool.pdf"
    Public FolderReceiverProject As String = "C:\temp\SwecoExportSignalDataXML\" & GetUserName() 'Automatisk skapa temp mapp för snabbare beräkningar

    Public ProofOfDelivery As String = "\\segvxfs001\PROJEKT\7656\7501425\000\3. Genomförande (E)\3.1 Arbetsmaterial\Mallar Leverans XML\Leveransbrev BIS 1.0.xlsx" 'T-disk
    Public xlApp As New Excel.Application 'starta applikation exel
    Public ProofOfDelivery_xlWorkBook As Excel.Workbook ' excelfil
    Public xlWorksheet As Excel.Worksheet 'flik i excelfil
    Public AllRow As Double = 0

    Public PrintTMoves As Boolean = False 'finns utskrift för verktyg TMoves
    Public PrintVTool As Boolean = False 'finns utskrift för verktyg VTool

    'VTools
    Public folder As String = "" 'lokal katalog för skapat projekt.
    Public AntalProjekteringarVTool As Integer = 0 ''antal projekt VTools
    Public importBIS_pl_config As String = "BIS_pl_config.csv" 'Platser
    Public importRoadsOfRoadParts As String = "RoadsOfRoadParts.csv" 'Vägar av vägdelar
    Public importRoadParts As String = "RoadParts.csv" 'Vägdelar
    Public importFrontProtection As String = "FrontProtection.csv" 'Frontskydd
    Public importConflictingRoutes_tv As String = "ConflictingRoutes_tv.csv" 'Otillåtna samtidigheter efter tågvägs slutpunkt
    Public importConflictingRoutes_vv As String = "ConflictingRoutes_vv.csv" 'Otillåtna samtidigheter efter växlingsväg slutpunkt
    Public importTC_T1_Mapping As String = "TC_T1_Mapping.csv" 'Mappning spårledningar
    Public Const importBacktoBack As String = "BackToBack_Signals.csv"
    Public exportXML As String = "VToolsSignalData.xml" 'sammanställning av samtliga CSV-filer
    Public importFolderVTool As String = "" 'Importmapp valt projekt manuellt
    Public importDatabasVTool As String = "" 'Databas valt projekt manuellt
    Public importUserDrawingVTool As String = "" 'Grafisk instruktionsritning valt projekt manuellt

    'Export XML-filer till Trafikverket
    Public OutlookNewSend As Boolean = False 'Skicka e-post direkt utan öppna Microsoft Outlook
    Public SwecoPortalDeliveryFilesZIP As Boolean = False 'XML katalog zip till Sweco Portal Leverans
    Public SwecoPortalDeliveryLetterPDF As Boolean = False 'Leveransbrev PDF till Sweco Portal Leverans
    Public OutlookDeliveryLetterPDF As Boolean = False 'Leveransbrev PDF till Microsoft Outlook
    Public OutlookXMLDeliveryFilesZIP As Boolean = False 'XML katalog zip till Microsoft Outlook
    Public OutlookXMLFiles As Boolean = False 'Bifoga XML filer till Microsoft Outlook

#End Region

#Region "Skapa mapp för projekt leverans av XML-filer till BIS"
    Public Function ProjectBISCatalog_Root(ByVal Projectfilefolder As String) As Boolean
        Dim createFolder = False

        Console.WriteLine("Projektfolder projekt: " & Projectfilefolder)
        Console.WriteLine("")

        Try
            ' Determine whether the directory exists. 
            If Directory.Exists(Projectfilefolder) Then
                MessageBox.Show("Mapp finns redan. " & Projectfilefolder)
                Exit Try
            End If

            ' Try to create the directory. 
            Dim di As DirectoryInfo = Directory.CreateDirectory(Projectfilefolder)
            createFolder = True

        Catch e As Exception
            MessageBox.Show("Skapande av Mapp misslyckades. " & e.ToString())
        End Try

        Return createFolder
    End Function
#End Region

#Region "Kopiera fil och spara i projektkatalog"
    Public Function ProofOfDelivery_ToCopyAlways(ByVal FileToCopy As String, ByVal FileSaveAs As String) As Boolean
        Dim createFolder = False

        Console.WriteLine("Kopierar CSV-fil: " & FileToCopy)
        Console.WriteLine("Till : " & FileSaveAs)


        Try
            ' Will not overwrite if the destination file already exists.
            File.Copy(Path.Combine(FileToCopy), Path.Combine(FileSaveAs))
            createFolder = True

            'File finns inte skapad. 
        Catch ex As IOException
            MessageBox.Show("VTools CSV-fil: " & vbCrLf & FileToCopy & vbCrLf &
                                        " saknas vid generering leverans XML-data." & vbCrLf & vbCrLf &
                                        "File måste vara skapat för fortsatt generering av XML-fil. Genereringen avbryts.", MsgBoxStyle.OkOnly)
            'Ingen referens. 
        Catch ex As NullReferenceException
            MessageBox.Show("NullReferenceException: " & ex.Message)
            MessageBox.Show("Stack Trace: " & vbCrLf & ex.StackTrace)

            'annan felorsak
        Catch ex As Exception
            MessageBox.Show("Fel vid hämtning/kopiering av VTool CSV-fil. " & ex.Message, MsgBoxStyle.OkOnly)
        End Try

        Return createFolder
    End Function


    Public Function ProofOfDelivery_ToCopySelected(ByVal FileToCopy As String, ByVal FileSaveAs As String) As Boolean
        Dim createFolder = False

        Console.WriteLine("Kopierar CSV-fil: " & FileToCopy)
        Console.WriteLine("Till : " & FileSaveAs)

        Try
            ' Will not overwrite if the destination file already exists.
            File.Copy(Path.Combine(FileToCopy), Path.Combine(FileSaveAs))
            createFolder = True

            'File finns inte skapad. 
        Catch ex As IOException
            Select Case MsgBox("VTools CSV-fil: " & vbCrLf & FileToCopy & vbCrLf &
                                  " saknas vid generering leverans XML-data." & vbCrLf & vbCrLf &
                                  "Vill du fortsätta att skapa en leverans ända ?", MsgBoxStyle.YesNo)

                Case MsgBoxResult.Yes
                    createFolder = True

                Case MsgBoxResult.No
                    createFolder = False
            End Select

            'Ingen referens. 
        Catch ex As NullReferenceException
            MessageBox.Show("NullReferenceException: " & ex.Message)
            MessageBox.Show("Stack Trace: " & vbCrLf & ex.StackTrace)

            'annan felorsak
        Catch ex As Exception
            MessageBox.Show("Fel vid hämtning/kopiering av VTool CSV-fil. " & ex.Message, MsgBoxStyle.OkOnly)
        End Try

        Return createFolder
    End Function
#End Region

#Region "Avsluta Excel"
    Public Sub CloseExcel(ByVal QuitExcel As Excel.Application, ByVal CloseBook As Excel.Workbook)
        Try
            CloseBook.Save()
            CloseBook.Close()
            QuitExcel.Quit()
        Catch ex As Exception
            Throw New ApplicationException("CloseExcel fail :" & ex.Message)
        End Try
    End Sub
#End Region

#Region "Skriv till Excelflik Leverans, Datum"
    Public Sub WriteswDateDataExcel(ByVal DeliveryWorksheet As Excel.Worksheet, ByVal swDate As String)
        Try
            DeliveryWorksheet.Range("G3").Value = swDate
        Catch ex As Exception
            Throw New ApplicationException("Write SignalData to Excel delivery fail :" & ex.Message)
        End Try
    End Sub
#End Region

#Region "Skriv till Excelflik Leverans, UPPRÄTTAD AV"
    Public Sub WriteswAuthorDataExcel(ByVal DeliveryWorksheet As Excel.Worksheet, ByVal swAuthor As String)
        Try
            DeliveryWorksheet.Range("D5").Value = swAuthor
        Catch ex As Exception
            Throw New ApplicationException("Write SignalData to Excel delivery fail :" & ex.Message)
        End Try
    End Sub
#End Region

#Region "Skriv till Excelflik POS, Stationsid"
    Public Sub WriteSignalPosDataExcel(ByVal DeliveryWorksheet As Excel.Worksheet, ByVal SignalData As String, ByVal RowPosition As Integer)
        Try
            DeliveryWorksheet.Cells(RowPosition, 1).Value = SignalData
        Catch ex As Exception
            Throw New ApplicationException("Write SignalPOS to Excel delivery fail :" & ex.Message)
        End Try
    End Sub
#End Region

#Region "Skriv till Excelflik Leverans, Stationsid"
    Public Sub WriteSignalStationDataExcel(ByVal DeliveryWorksheet As Excel.Worksheet, ByVal SignalData As String, ByVal RowPosition As Integer)
        Try
            DeliveryWorksheet.Cells(RowPosition, 2).Value = SignalData
        Catch ex As Exception
            Throw New ApplicationException("Write SignalStation to Excel delivery fail :" & ex.Message)
        End Try
    End Sub
#End Region

#Region "Skriv till Excelflik Leverans, filnamn XML"
    Public Sub WriteSignalFileXMLDataExcel(ByVal DeliveryWorksheet As Excel.Worksheet, ByVal SignalData As String, ByVal RowPosition As Integer)
        Try
            DeliveryWorksheet.Cells(RowPosition, 3).Value = SignalData
        Catch ex As Exception
            Throw New ApplicationException("Write SignalFileXML to Excel delivery fail :" & ex.Message)
        End Try
    End Sub
#End Region

#Region "Skriv till Excelflik Leverans, typ av projekt"
    Public Sub WriteSignalTypeOfProjectDataExcel(ByVal DeliveryWorksheet As Excel.Worksheet, ByVal SignalData As String, ByVal RowPosition As Integer)
        Try
            DeliveryWorksheet.Cells(RowPosition, 8).Value = SignalData
        Catch ex As Exception
            Throw New ApplicationException("Write SignalData to Excel delivery fail :" & ex.Message)
        End Try
    End Sub
#End Region

#Region "Skriv till Excelflik Leverans, Rubrik Bandel"
    Public Sub WriteSignalTrackPortionDataExcel(ByVal DeliveryWorksheet As Excel.Worksheet, ByVal SignalData As String, ByVal RowPosition As Integer)
        Try
            DeliveryWorksheet.Cells(RowPosition, 4).Value = SignalData
        Catch ex As Exception
            Throw New ApplicationException("Write SignalData to Excel delivery fail :" & ex.Message)
        End Try
    End Sub
#End Region

#Region "Skriv till Excelflik Kvalitetskontroll, Verifiering"
    Public Sub WriteQualityControlVerifieringDataExcel(ByVal DeliveryWorksheet As Excel.Worksheet,
                                                       ByVal DriftId As String,
                                                       ByVal ClassData As String,
                                                       ByVal ClassErrors As Integer,
                                                       ByVal SignalData As String,
                                                       ByVal RowPosition As Integer)
        Try
            DeliveryWorksheet.Cells(RowPosition + 1, 1).Value = RowPosition ' position rad i excel
            DeliveryWorksheet.Cells(RowPosition + 1, 2).Value = DriftId ' driftplats ID
            DeliveryWorksheet.Cells(RowPosition + 1, 3).Value = ClassErrors 'klassning av fel
            DeliveryWorksheet.Cells(RowPosition + 1, 4).Value = ClassData ' typ av fel
            DeliveryWorksheet.Cells(RowPosition + 1, 5).Value = SignalData 'beskrivning
        Catch ex As Exception
            Throw New ApplicationException("Write Kvalitetskontroll to Excel delivery fail :" & ex.Message)
        End Try
    End Sub
#End Region

#Region "Läsa av vilka driftplatser som ingår i vald bandel VTools"
    Public Sub ReadStationsdataVTools(ByVal TableData As String)
        Dim index_DriftplatsPost As Integer = 2
        Dim Excel_TotalRows As Integer = 0
        Dim totaldriftplatser As Integer = 0

        'startvärde antal projekt
        AntalProjekteringarVTool = 0

        Try
            'totalt antal rader i exelfil
            Excel_TotalRows = ListOperationSite_xlWorksheet.UsedRange.Rows.Count

            'öppna flik Resultat
            ListOperationSite_xlWorksheet = ListOperationSite_xlWorkBook.Worksheets("Resultat")

            Console.WriteLine("--- VTools Excelfil läs av Driftplatser och linjer ---")
            Console.WriteLine("Antal rader i Excelfil:" & Excel_TotalRows)
            Console.WriteLine("Sök i exelfil efter bandelsnummer :" & TableData)

            'sök igenom efter driftplatsnummer
            Do While index_DriftplatsPost <= Excel_TotalRows

                Dim textfield_objnr = ListOperationSite_xlWorksheet.Range("A" & index_DriftplatsPost).Value.ToString 'obnr
                Dim textfield_Signatur = ListOperationSite_xlWorksheet.Range("B" & index_DriftplatsPost).Value.ToString 'Signatur
                Dim textfield_Platsnamn = ListOperationSite_xlWorksheet.Range("C" & index_DriftplatsPost).Value.ToString 'Platsnamn
                Dim textfield_Platstyp = ListOperationSite_xlWorksheet.Range("D" & index_DriftplatsPost).Value.ToString 'Platstyp
                Dim textfield_Bandel = ListOperationSite_xlWorksheet.Range("E" & index_DriftplatsPost).Value.ToString 'Bandel
                Dim textfield_Skrivut = ListOperationSite_xlWorksheet.Range("G" & index_DriftplatsPost).Value.ToString 'skriv ut VTool projekt J/N

                'vald driftplatsnummer finns i excelfil
                If textfield_Bandel = TableData Then

                    'Driftplatsnummer ska skrivas ut
                    If textfield_Skrivut = "J" Then

                        'sökväg till VTool projekt
                        Dim textfield_VToolProjekt = ListOperationSite_xlWorksheet.Range("H" & index_DriftplatsPost).Value.ToString
                        If textfield_VToolProjekt <> "" Then

                            'spara i array för utskrift XML
                            ArrayStations(SwTmovesArrayStationsPost, 0) = textfield_objnr
                            ArrayStations(SwTmovesArrayStationsPost, 1) = textfield_Platsnamn
                            ArrayStations(SwTmovesArrayStationsPost, 2) = textfield_Platstyp
                            ArrayStations(SwTmovesArrayStationsPost, 3) = ""
                            ArrayStations(SwTmovesArrayStationsPost, 4) = textfield_Signatur
                            ArrayStations(SwTmovesArrayStationsPost, 5) = textfield_Bandel
                            ArrayStations(SwTmovesArrayStationsPost, 6) = "VTool"
                            ArrayStations(SwTmovesArrayStationsPost, 7) = textfield_VToolProjekt

                            Console.WriteLine("ID: " & textfield_objnr)
                            Console.WriteLine("StationName: " & textfield_Platsnamn)
                            Console.WriteLine("Type: " & textfield_Platstyp)
                            Console.WriteLine("Desc: " & ArrayStations(SwTmovesArrayStationsPost, 3))
                            Console.WriteLine("ShortName: " & textfield_Signatur)
                            Console.WriteLine("TrackSection: " & textfield_Bandel)
                            Console.WriteLine("Projekteringsverktyg: " & "VTool")
                            Console.WriteLine("VToolProjekt: " & textfield_VToolProjekt)
                            Console.WriteLine("")

                            SwTmovesArrayStationsPost += 1
                            totaldriftplatser += 1
                            AntalProjekteringarVTool += 1

                        Else
                            MessageBox.Show("Sökväg till VTool projekt " & TableData & " saknas. " & textfield_VToolProjekt, "VTool", MessageBoxButton.OK, MessageBoxImage.Warning)
                            Exit Sub

                        End If
                    End If
                End If

                index_DriftplatsPost += 1
            Loop

            Console.WriteLine("Antal driftplatser och linjer i verktyg VTools " & totaldriftplatser &
                              ". Totalt antal driftplatser " & SwTmovesArrayStationsPost)
            Console.WriteLine("")

        Catch ex As Exception
            Throw New ApplicationException("Avläsning av lokal fil Projekteringsunderlag för VTool avbruten :" & ex.Message)
        End Try
    End Sub
#End Region

#Region "Läs av Excelfil driftplats för verktyg VTool"
    Public Sub ReadOnlyStationsdataVTools(ByVal TableData As String)
        Dim index_DriftplatsPost As Integer = 2
        Dim Excel_TotalRows As Integer = 0
        Dim totaldriftplatser As Integer = 0

        Try
            'totalt antal rader i exelfil
            Excel_TotalRows = ListOperationSite_xlWorksheet.UsedRange.Rows.Count

            'öppna flik Resultat
            ListOperationSite_xlWorksheet = ListOperationSite_xlWorkBook.Worksheets("Resultat")

            Console.WriteLine("--- VTools Excelfil läs av Driftplatser och linjer ---")
            Console.WriteLine("Antal rader i Excelfil:" & Excel_TotalRows)
            Console.WriteLine("Sök i exelfil efter driftplatsnummer :" & TableData)

            'sök igenom efter driftplatsnummer
            Do While index_DriftplatsPost <= Excel_TotalRows

                Dim textfield_objnr = ListOperationSite_xlWorksheet.Range("A" & index_DriftplatsPost).Value.ToString 'obnr
                Dim textfield_Signatur = ListOperationSite_xlWorksheet.Range("B" & index_DriftplatsPost).Value.ToString 'Signatur
                Dim textfield_Platsnamn = ListOperationSite_xlWorksheet.Range("C" & index_DriftplatsPost).Value.ToString 'Platsnamn
                Dim textfield_Platstyp = ListOperationSite_xlWorksheet.Range("D" & index_DriftplatsPost).Value.ToString 'Platstyp
                Dim textfield_Bandel = ListOperationSite_xlWorksheet.Range("E" & index_DriftplatsPost).Value.ToString 'Bandel
                Dim textfield_Skrivut = ListOperationSite_xlWorksheet.Range("G" & index_DriftplatsPost).Value.ToString 'skriv ut VTool projekt J/N

                'vald driftplatsnummer finns i excelfil
                If textfield_objnr = TableData Then

                    'Driftplatsnummer ska skrivas ut
                    If textfield_Skrivut = "J" Then

                        'sökväg till VTool projekt saknas
                        Dim textfield_VToolProjekt = ListOperationSite_xlWorksheet.Range("H" & index_DriftplatsPost).Value.ToString
                        If textfield_VToolProjekt <> "" Then

                            'spara i array för utskrift XML
                            ArrayStations(SwTmovesArrayStationsPost, 0) = textfield_objnr
                            ArrayStations(SwTmovesArrayStationsPost, 1) = textfield_Platsnamn
                            ArrayStations(SwTmovesArrayStationsPost, 2) = textfield_Platstyp
                            ArrayStations(SwTmovesArrayStationsPost, 3) = ""
                            ArrayStations(SwTmovesArrayStationsPost, 4) = textfield_Signatur
                            ArrayStations(SwTmovesArrayStationsPost, 5) = textfield_Bandel
                            ArrayStations(SwTmovesArrayStationsPost, 6) = "VTool"
                            ArrayStations(SwTmovesArrayStationsPost, 7) = textfield_VToolProjekt

                            Console.WriteLine("ID: " & textfield_objnr)
                            Console.WriteLine("StationName: " & textfield_Platsnamn)
                            Console.WriteLine("Type: " & textfield_Platstyp)
                            Console.WriteLine("Desc: " & ArrayStations(SwTmovesArrayStationsPost, 3))
                            Console.WriteLine("ShortName: " & textfield_Signatur)
                            Console.WriteLine("TrackSection: " & textfield_Bandel)
                            Console.WriteLine("Projekteringsverktyg: " & "VTool")
                            Console.WriteLine("VToolProjekt: " & textfield_VToolProjekt)
                            Console.WriteLine("")

                            SwTmovesArrayStationsPost += 1
                            totaldriftplatser += 1

                            Console.WriteLine("Antal Stations i verktyg VTool :" & totaldriftplatser)
                            Console.WriteLine("")
                            Exit Sub

                        Else
                            MessageBox.Show("Sökväg till VTool projekt " & TableData & " saknas. " & textfield_VToolProjekt, "VTool", MessageBoxButton.OK, MessageBoxImage.Warning)
                            Exit Sub
                        End If
                    End If
                End If

                index_DriftplatsPost += 1
            Loop

            'driftplatsnummer saknas
            If index_DriftplatsPost > Excel_TotalRows Then
                MessageBox.Show("Valt driftplatsnummer saknas i projekteringsunderlag för VTool.", "Driftplatsnummer", MessageBoxButton.OK, MessageBoxImage.Warning)
                Exit Try
            End If

        Catch ex As Exception
            Throw New ApplicationException("Avläsning av lokal fil Projekteringsunderlag för VTool avbruten :" & ex.Message)
        End Try
    End Sub
#End Region

#Region "Skriv till textfil"
    Public Sub WriteDesignBasisFile(ByVal textfile As String, ByVal writetext As String)
        Try
            Using sw As StreamWriter = File.AppendText(textfile)
                sw.WriteLine(writetext)
            End Using
        Catch ex As Exception
            Throw New ApplicationException("Write to textfile delivery fail :" & ex.Message)
        End Try
    End Sub
#End Region

#Region "Avsluta/stäng textfil"
    Public Sub CloseDesignBasisFile(ByVal textfile As String)
        Try
            Using sw As StreamWriter = File.AppendText(textfile)
                sw.Close()
            End Using
        Catch ex As Exception
            Throw New ApplicationException("Close textfile delivery fail :" & ex.Message)
        End Try
    End Sub
#End Region

#Region "Flytta en hel mapp på samma enhet"
    Public Sub MoveDirSourceToDestination(ByVal sourceDirectory As String, ByVal destinationDirectory As String)
        Try
            'flytta mapp
            Directory.Move(sourceDirectory, destinationDirectory)

        Catch ex As Exception
            MessageBox.Show("Flytta en hel mapp på samma enhet...  " & ex.Message)
        End Try
    End Sub
#End Region

#Region "Flytta en hel mapp till annan enhet"
    Public Sub MoveDirectory(ByVal strSourceDir As String, ByVal strDestDir As String, Optional ByVal bDelSource As Boolean = True)
        If Directory.Exists(strSourceDir) Then
            If Directory.GetDirectoryRoot(strSourceDir) = Directory.GetDirectoryRoot(strDestDir) Then
                Directory.Move(strSourceDir, strDestDir)
            Else
                Try
                    CopyDirectory(New DirectoryInfo(strSourceDir), New DirectoryInfo(strDestDir))
                    If bDelSource = True Then
                        Directory.Delete(strSourceDir, True)
                    End If
                Catch subEx As Exception
                    MessageBox.Show("Flytta en hel mapp till annan enhet...  " & subEx.Message)
                End Try
            End If
        End If
    End Sub

    Private Sub CopyDirectory(ByVal diSourceDir As DirectoryInfo, ByVal diDestDir As DirectoryInfo)
        If diDestDir.Exists = False Then
            diDestDir.Create()
        End If
        Dim fiSrcFiles() As FileInfo = diSourceDir.GetFiles()
        For Each fiSrcFile As FileInfo In fiSrcFiles
            fiSrcFile.CopyTo(Path.Combine(diDestDir.FullName, fiSrcFile.Name))
        Next
        Dim diSrcDirectories() As DirectoryInfo = diSourceDir.GetDirectories()
        For Each diSrcDirectory As DirectoryInfo In diSrcDirectories
            CopyDirectory(diSrcDirectory, New DirectoryInfo(Path.Combine(diDestDir.FullName, diSrcDirectory.Name)))
        Next
    End Sub
#End Region

End Module
