﻿
Imports Microsoft.Office.Core
Imports Microsoft.Office.Interop
Imports Microsoft.Office.Interop.Excel
Imports RorelsevagarOchSkyddLib

Public Class ReviewXMLFilesANDA

#Region "Private"
    Private ReviewXMLAnda_xlApp As New Excel.Application 'starta applikation exel
    Private ReviewXMLAnda_xlWorkBook As Excel.Workbook ' excelfil
    Private ReviewXMLAnda_xlWorksheet As Excel.Worksheet 'excelflik
    Private ReviewXMLAnda_ExcelFile As String = "C:\VisualStudio\Projects\SwExportSignaldata\Utskrift\Mall\Granskning XML Anda.xlsx"
    Private ReviewXMLAnda_ExcelFileNew As String = "C:\VisualStudio\Projects\SwExportSignaldata\Utskrift\Mall\Granskning XML Anda New.xlsx"

    Private Flik_Rad As Integer = 30
#End Region

#Region " <!--Knapp Välj XML fil-->"
    Private Sub btReviewXMLFiles_Click(sender As Object, e As RoutedEventArgs)

    End Sub
#End Region

#Region " <!--Knapp Skriv ut-->"
    Private Sub vbSaveReviewXMLFiles_Click(sender As Object, e As RoutedEventArgs)
        Dim ReviewXMLAnda_XMLlFile As String = ""

        'Rad i vald flik

        'flik Slutpunkter och Frontskydd
        Dim Slutpunktobjekt_Kolumn As Integer = 0
        Dim Slutpunkt_Kolumn As Integer = 3
        Dim Frontskydd_Kolumn As Integer = 6
        Dim ObjektMedVillkor_Kolumn As Integer = 7

        'vald xml fil
        ReviewXMLAnda_XMLlFile = tbANDA.Text

        'kopiera execelfil Granskning XML Anda
        If Not ProofOfDelivery_ToCopyAlways(ReviewXMLAnda_ExcelFile, ReviewXMLAnda_ExcelFileNew) = True Then
            Exit Sub

        Else
            'öppna xml fil
            ReviewXMLAnda_xlWorkBook = ReviewXMLAnda_xlApp.Workbooks.Open(ReviewXMLAnda_ExcelFileNew)

            'skapa slutpunkt
            Dim root As Signaldata = New Signaldata

            'hämta xlm fil
            root.FromXmlFile(ReviewXMLAnda_XMLlFile)

            'slutpunkter
            If (Not root.Slutpunkter Is Nothing) Then

                'öppna flik Slutpunkter och Frontskydd
                ReviewXMLAnda_xlWorksheet = ReviewXMLAnda_xlWorkBook.Worksheets("SlutpunkterFrontskydd")

                'alla slutpunkter
                Dim str As SlutpunktsobjektA
                For Each str In root.Slutpunkter

                    Dim Bis_Objektnummer As String = ""
                    Dim BIS_Objekttypnummer As String = ""
                    Dim Anda_OID As String = ""
                    Dim Anda_VID As String = ""
                    Dim Ver_Signatur As String = ""
                    Dim Ver_Platssignatur As String = ""
                    Dim Ver_Objekttyp As String = ""

                    Dim Frislappningshastighet As String = ""
                    Dim DynamisktFrontskydsomrade As String = ""


                    Frislappningshastighet = str.Frislappningshastighet.ToString()
                    DynamisktFrontskydsomrade = str.DynamisktFrontskydsomrade.ToString()

                    If (Not str.SlutpunktsobjektId.BisObjektId Is Nothing) Then
                        Bis_Objektnummer = str.SlutpunktsobjektId.BisObjektId.Objektnummer
                        BIS_Objekttypnummer = str.SlutpunktsobjektId.BisObjektId.Objekttypnummer
                    End If

                    If (Not str.SlutpunktsobjektId.AndaObjektId Is Nothing) Then
                        Anda_OID = str.SlutpunktsobjektId.AndaObjektId.OID
                        Anda_VID = str.SlutpunktsobjektId.AndaObjektId.VID
                    End If

                    If (Not str.SlutpunktsobjektId.VerksamhetsId Is Nothing) Then
                        Ver_Signatur = str.SlutpunktsobjektId.VerksamhetsId.Signatur
                        Ver_Platssignatur = str.SlutpunktsobjektId.VerksamhetsId.Platssignatur
                        Ver_Objekttyp = str.SlutpunktsobjektId.VerksamhetsId.Objekttyp
                    End If

                    'Rubrik och fet text
                    'WriteHeading_ANDA(ReviewXMLAnda_xlWorksheet, Flik_Rad)

                    'slutpunktsobjekt
                    WriteSlutpunkterObjekt(Bis_Objektnummer, BIS_Objekttypnummer,
                                           Anda_OID, Anda_VID,
                                           Ver_Signatur, Ver_Platssignatur, Ver_Objekttyp,
                                           Frislappningshastighet, DynamisktFrontskydsomrade,
                                           ReviewXMLAnda_xlWorksheet, Flik_Rad)

                    'frontskydd

                    If (Not root.Borjanpunkter Is Nothing) Then

                    End If


                    'Dim fr As FrontskyddA


                    'WriteOmr2_ANDA(ReviewXMLAnda_xlWorksheet, str.Frislappningshastighet.ToString(), str.DynamisktFrontskydsomrade.ToString(), Flik_Rad)

                    Flik_Rad += 2
                Next



            End If



            'spara
            ReviewXMLAnda_xlWorkBook.Save()

            'stäng alla flikar
            ReviewXMLAnda_xlWorkBook.Close()

            'avsluta kopplingen till Excel
            ReviewXMLAnda_xlApp.Quit()

            MessageBox.Show("Granskningslista klart.")
        End If


    End Sub

    Private Sub WriteSlutpunkterObjekt(ByVal bis_Objektnummer As String, ByVal bIS_Objekttypnummer As String, ByVal anda_OID As String, ByVal anda_VID As String,
                                       ByVal ver_Signatur As String, ByVal ver_Platssignatur As String, ByVal ver_Objekttyp As String,
                                       ByVal frislappningshastighet As String, ByVal dynamisktFrontskydsomrade As String,
                                       ByVal reviewXMLAnda_xlWorksheet As Worksheet, ByRef flik_Rad As Integer)
        'rad 1
        reviewXMLAnda_xlWorksheet.Cells(flik_Rad, 1).Value = "Slutpunktobjekt"

        'rad 2
        reviewXMLAnda_xlWorksheet.Cells(flik_Rad + 1, 1).Value = "BIS Id"
        reviewXMLAnda_xlWorksheet.Cells(flik_Rad + 1, 4).Value = "Anda Id"
        reviewXMLAnda_xlWorksheet.Cells(flik_Rad + 1, 7).Value = "Verksamhets Id"
        reviewXMLAnda_xlWorksheet.Cells(flik_Rad + 1, 10).Value = "Frisläpphastig"
        reviewXMLAnda_xlWorksheet.Cells(flik_Rad + 1, 11).Value = "Dynamiskt"

        'rad 3
        reviewXMLAnda_xlWorksheet.Cells(flik_Rad + 2, 1).Value = "Objnr"
        reviewXMLAnda_xlWorksheet.Cells(flik_Rad + 2, 2).Value = bis_Objektnummer
        reviewXMLAnda_xlWorksheet.Cells(flik_Rad + 2, 4).Value = "Objnr"
        reviewXMLAnda_xlWorksheet.Cells(flik_Rad + 2, 5).Value = anda_OID
        reviewXMLAnda_xlWorksheet.Cells(flik_Rad + 2, 7).Value = "Signalnamn"
        reviewXMLAnda_xlWorksheet.Cells(flik_Rad + 2, 5).Value = ver_Signatur
        reviewXMLAnda_xlWorksheet.Cells(flik_Rad + 2, 10).Value = frislappningshastighet
        reviewXMLAnda_xlWorksheet.Cells(flik_Rad + 2, 11).Value = dynamisktFrontskydsomrade

        'rad 4
        reviewXMLAnda_xlWorksheet.Cells(flik_Rad + 3, 1).Value = "Objtyp"
        reviewXMLAnda_xlWorksheet.Cells(flik_Rad + 3, 2).Value = bIS_Objekttypnummer
        reviewXMLAnda_xlWorksheet.Cells(flik_Rad + 3, 4).Value = "Objtyp"
        reviewXMLAnda_xlWorksheet.Cells(flik_Rad + 3, 5).Value = anda_VID
        reviewXMLAnda_xlWorksheet.Cells(flik_Rad + 3, 7).Value = "Platssignatur"
        reviewXMLAnda_xlWorksheet.Cells(flik_Rad + 3, 5).Value = ver_Platssignatur

        'rad 5
        reviewXMLAnda_xlWorksheet.Cells(flik_Rad + 3, 7).Value = "Objekttyp"
        reviewXMLAnda_xlWorksheet.Cells(flik_Rad + 3, 5).Value = ver_Objekttyp

        flik_Rad += 5

    End Sub

    Private Sub WriteOmr2_ANDA(ByVal reviewXMLAnda_xlWorksheet As Worksheet, ByVal toString1 As String, ByVal toString2 As String, ByVal flik_Rad As Integer)
        Try
            reviewXMLAnda_xlWorksheet.Cells(flik_Rad + 9, 1).Value = "Frisläpphastig"
            reviewXMLAnda_xlWorksheet.Cells(flik_Rad + 9, 1).Value = "Dynamiskt"
            reviewXMLAnda_xlWorksheet.Cells(flik_Rad + 9, 2).Value = toString1
            reviewXMLAnda_xlWorksheet.Cells(flik_Rad + 9, 2).Value = toString2
        Catch ex As Exception
            Throw New ApplicationException("Write Heading to Excel delivery fail :" & ex.Message)
        End Try


    End Sub
#End Region

    Private Sub WriteHeading_ANDA(ByVal reviewXMLAnda_xlWorksheet As Worksheet, ByVal flik_Rad As Integer)
        Try
            reviewXMLAnda_xlWorksheet.Cells(flik_Rad, 1).Value = "Slutpunktobjekt"
            reviewXMLAnda_xlWorksheet.Cells(flik_Rad, 4).Value = "Slutpunkt"
            reviewXMLAnda_xlWorksheet.Cells(flik_Rad, 7).Value = "Frontskydd"
            reviewXMLAnda_xlWorksheet.Cells(flik_Rad, 8).Value = "Objekt med villkor"
        Catch ex As Exception
            Throw New ApplicationException("Write Heading to Excel delivery fail :" & ex.Message)
        End Try
    End Sub
End Class
